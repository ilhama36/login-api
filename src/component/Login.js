import React, {Component} from 'react';
import { connect } from 'react-redux';
import { addUserName } from '../reducer/User';
import { Firebase } from '.././config';
import {Link, withRouter} from 'react-router-dom';
import 'firebase/database';
import * as firebase from 'firebase/app';

class Login extends Component{
	constructor(props){
		super(props);
		this.state = {
			username: "",
			password: "",
			isLoading: false
		}
		this.handler.bind(this)
		this.login.bind(this)
		this.checkData.bind(this)
	}

	componentDidMount(){
		this.checkData()
	}

	checkData = () => {
	const that = this;
		Firebase.auth().onAuthStateChanged(function(user){
		  if (user) {
		    // User is signed in.
		    //console.log({this: this, that})
		    that.props.history.push('/home')
		  }
		});
	}
	handler = (e) => {
		e.preventDefault()
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	login = async() =>{
		const user = this.props.addUserName
		const { username, password } = this.state
		let userResult = username + "@gmail.com"
		try {
            await Firebase.auth().signInWithEmailAndPassword(userResult, password)
            .then(() => {
            	user(username)
                this.props.history.push('/home')
            }) 
            await Firebase.auth().onAuthStateChanged((Profile) => {
            	if(Profile.displayName === null){
            		var user = firebase.auth().currentUser;
            		user.updateProfile({
            			displayName: username
        			})
            	}
            })
        }
        catch(error){
            alert(error.message, error.code)
        }	
	}

	render(){
		return(
			<div>
				<h3 className="text-center text-white pt-5">Login form</h3>
		        <div className="container">
		            <div id="login-row" className="row justify-content-center align-items-center">
		                <div id="login-column" className="col-md-6">
		                    <div id="login-box" className="col-md-12">
	                            <h3 className="text-center text-info">Login</h3>
	                            <div className="form-group">
	                                <label className="text-info">Username:</label>
	                                <input type="text" name="username" className="form-control" onChange={this.handler} required/>
	                            </div>
	                            <div className="form-group">
	                                <label className="text-info">Password:</label>
	                                <input type="password" name="password" className="form-control" onChange={this.handler} required/>
	                            </div>
	                            <div className="form-group">
	                                <label className="text-info"><span>Remember me</span> 
	                                <span><input id="remember-me" name="remember-me" type="checkbox" /></span></label><br />
	                                <input type="submit" name="submit" className="btn btn-info btn-md" value="submit" onClick={this.login} />
	                            </div>
	                            <div id="register-link" className="text-right" >
	                                <Link to={'/register'} className="text-info">Register here</Link>
	                            </div>
		                    </div>
		                </div>
		            </div>
		        </div>
	        </div>
		)	
	}
	
}

const mapDispatchToProps = (dispatch) => {
  return {
    addUserName: username => {
       dispatch(addUserName(username))
    }
  }
}
const mapStateToProps = (state) => {
  return {
    User: state.User.username
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))