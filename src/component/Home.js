import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Firebase} from '.././config';
import { addUserName } from '../reducer/User';

class Home extends Component {
	constructor(){
		super();
		this.LogOut.bind(this);
	}

	componentDidMount(){
		const user = this.props.addUserName
		const that = this
		Firebase.auth().onAuthStateChanged(function(Profile) {
		  if (Profile) {
		    // User is signed in.
		    user(Profile && Profile.displayName)
		  } else {
		    // No user is signed in.
		    that.props.history.push('/')
		  }
		});
	}

	LogOut = () => {
		Firebase.auth().signOut()
		.then(() => {
			console.log("Bye")
		})
		.catch((error) => {
			console.log(error)
		})
	}

	render(){
		const { User } = this.props
		return(
			<>	
				<div className="container">
					<h1>Halo {User}</h1>
					<div className="btn">
						<input type="submit" value="LogOut" onClick={this.LogOut} />
					</div>
				</div>
			</>
		)
	}
}

const mapStateToProps = (state) => {
  return {
    User: state.User.username.name
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addUserName: username => {
       dispatch(addUserName(username))
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home)