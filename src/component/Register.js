import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Firebase } from '.././config';

export default class Register extends Component{
	constructor(props){
		super(props);
		this.state = {
			password: "",
			email: "",
			isLoading: false
		}
		this.inputHandler.bind(this);
		this.register.bind(this);
	}

	inputHandler = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	register = async(e) => {
		e.preventDefault();
		const { username, password } = this.state
		let confirmPass = document.getElementById("confirm-password").value
		let userResult = username + "@gmail.com";
		password !== confirmPass ? 
		alert("Password dan Confirm Password tidak Sama") : 	
		await Firebase.auth().createUserWithEmailAndPassword(userResult, password)
		.then(() => {
			this.props.history.push('/')
		})
		.catch((error) =>{
			alert(error.message)
		})

	}

	render(){
		return(
			<div>
				<h3 className="text-center text-white pt-5">Register form</h3>
		        <div className="container">
		            <div id="login-row" className="row justify-content-center align-items-center">
		                <div id="login-column" className="col-md-6">
		                    <div id="login-box" className="col-md-12">
	                            <h3 className="text-center text-info">Register</h3>
	                            <form>
		                            <div className="form-group">
		                                <label className="text-info">Username:</label>
		                                <input type="text" name="username" className="form-control" onChange={this.inputHandler} required/>
		                            </div>
		                            <div className="form-group">
		                                <label className="text-info">Password:</label>
		                                <input type="password" name="password" className="form-control" onChange={this.inputHandler} required/>
		                            </div>
		                            <div className="form-group">
		                                <label className="text-info">Confirmasi Password:</label>
		                                <input type="password" id="confirm-password" className="form-control" required/>
		                            </div>
		                            <div className="form-group">
		                                <input type="submit" name="submit" className="btn btn-info btn-md" value="submit" onClick={this.register} />
		                            </div>
		                            <div id="register-link" className="text-right">
		                                <Link to={'/'} className="text-info">Already Have an Account ? Click here</Link>
		                            </div>
	                            </form>
		                    </div>
		                </div>
		            </div>
		        </div>
	        </div>
		)
	}
}