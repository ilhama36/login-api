import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from "react-router-dom";
import store from './store';
import Home from './component/Home';
import Register from './component/Register';
import Login from './component/Login';
import './App.css';

export default class App extends Component {
  render() {
    return (
    <Provider store={store}>
      <Router>
        <div id="app">
          <Route exact path="/" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/home" component={Home} />
        </div>
      </Router>
    </Provider>
    );
  }
}