import * as firebase from "firebase/app";
import 'firebase/storage';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyBwZZvIWijZAoGtqtm5d5UVwTN-t0iXdzg",
  authDomain: "api-project-503673844566.firebaseapp.com",
  databaseURL: "https://api-project-503673844566.firebaseio.com",
  projectId: "api-project-503673844566",
  storageBucket: "api-project-503673844566.appspot.com",
  messagingSenderId: "503673844566",
  appId: "1:503673844566:web:d1249131d6128b07"
};

export const Firebase = firebase.initializeApp(firebaseConfig);

const Storage = firebase.storage();


export {
  Storage,
  firebase as default
}